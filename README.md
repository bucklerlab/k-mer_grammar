**kgrammar**

This is the repository for the code described in the paper:
**A *k*-mer grammar analysis to uncover maize regulatory architecture**

The public and current version of the paper can be seen in  
[BMC Plant Biology](https://bmcplantbiol.biomedcentral.com/articles/10.1186/s12870-019-1693-2)

---

## Requirements

Code in this repository requires Python3 and has been tested in CentOS Linux 7 and macOS High Sierra Version 10
---

## About the input datasets
Databases are provided in the folder input_databases
For each dataset (i.e., FEA4, KN1, coreprom, mnaseqshoot, mnaseqroot) there is a folder that contains the
sqlite3 file **data_model.db**

Each data_model.db file include test and train tables
with the following columns
"chr" INTEGER,
"left_idx" INTEGER,
"right_idx" INTEGER,
"dna_string" CHAR,
"bound" INTEGER

---

## About controls
* Each data_model.db file includes test and train tables.
* Each data_model.db file contains both positive and control sequences.
* Positive sequences appear associated with a value of 1 in the bound column.
* Control sequences appear associated with a value of 0 in the bound column.

---

## Train and test a "bag-of-*k*-mers" model
To find how to train a "bag-of-*k*-mers" model type:
python kgrammar_bag-of-k-mer_training_testing.py -help

To get:
Usage: python kgrammar_bag-of-k-mer_training_testing.py [kmersize, integer] [mode filtered, 'True', or 'False' (i.e., mode full)] [dataset_name, string]
Example: python kgrammar_bag-of-k-mer_training_testing.py 8 False FEA4

The above example will train a model with k=8, without filtering k-mers by complexity and reading the file under input_database/FEA4/data_model.db
The resulting model file will be saved, together with a database that contains k-mer weights.

After training the model, the script with proceed to test and save ROC and PR curves
Results of the training for accuracy and others metrics (including confusion matrix) will be saved to the log file
--

## Train and test a "vector-*k*-mers" model
To find how to train a "vector-*k*-mers" model type:
python kgrammar_vector-k-mer_training_testing.py -help

To get:
Usage: python kgrammar_vector-k-mers_training_testing.py [kmersize, integer] [windowsize, integer] [kmer_parsing, 'True' for kmer, 'False' for newtokens] [dataset_name, string]
Example: python kgrammar_vector-k-mers_training_testing.py 8 5 False FEA4

The above example will train a model with k=8, window size=5, using uncollapsed k-mers and reading the file under input_database/FEA4/data_model.db
The vectors for the positive and control sequences will be saved.

After training the model, the script with proceed to test and save ROC and PR curves
Results of the training for accuracy and others metrics (including confusion matrix) will be saved to the log file
--

## Authors

This software was written by Maria Katherine Mejia-Guerra. Please feel free to submit any bugs or feature requests.
If you try it, I'd also love to hear about your experiences in general. Drop me an email!

--

## References
The approaches implemented here were borrowed from the field of Natural Language Processing. 
In addition, similar ideas have been explored in genomics as this is an active area of research.